local widget = require ("widget")
local scrollView = widget.newScrollView (
		{
			top = display.topStatusBarContentHeight
			,width = display.contentWidth
			,height = display.contentHeight - display.topStatusBarContentHeight
			,scrollWidth = display.contentWidth
			,scrollHeight = 5*display.contentHeight
			,listener = scrollListener
			,horizontalScrollDisabled = true
		}
	)

-- Высота изображения
scrollView.imageHeight = 350

-- Отступ изображения от правого и левого края скролла
scrollView.hPadding = 20

-- Расстояние между изображениями
scrollView.spacing = 20

-- Колличество изображений в списке
scrollView.count = 0

-- Слушатель загрузки изображения из сети
local function networkListener( self, event )
    local listener = self.listener
 
	if not event.isError then 
		if event.phase == "ended" then
			print ("Image was loaded. Lets append it'")

			local width = scrollView.width - 2*scrollView.hPadding
			local height = scrollView.imageHeight
			local image = display.newImage(self.filename, self.baseDir, 0, 0)  

			-- Вписывам изображение в делегат с сохранением пропорций
			local cw = width / image.width
			local ch = height / image.height

			if cw < ch then
				image.width = width
				image.height = cw * image.height
            else 
				image.width = ch * image.width
				image.height = height
			end

			image.x = 0.5*scrollView.width
			if self.index > 0 then
				image.y = self.index * scrollView.imageHeight  + scrollView.spacing + 0.5*height
			else 
				image.y = 0.5*height
			end

			scrollView:insert (image)
		end
	else
		print ("Error of loading image")
		-- TODO Добавить элемент незагруженного изображения
    end
end
 
-- Добавляет изображение
function scrollView.append (url)
	local method = "GET"
	local index = scrollView.count
	local destFilename = "exam_gmsoft_"..index..".png"
	local baseDir = system.TemporaryDirectory
 
    if (destFilename) then
        local options = {
            filename = destFilename, 
            baseDir = baseDir,
            networkRequest = networkListener, 
			index = scrollView.count
        }    
 
		network.download(url, method, options, destFilename, baseDir)
    else
        print( "ERROR: no destination filename supplied to display.loadRemoteImage()" )
    end

	scrollView.count = scrollView.count + 1
end

return scrollView